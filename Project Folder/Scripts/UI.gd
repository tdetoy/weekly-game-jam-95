extends Control

var stadium
var level
var total_time
var time_left
onready var player_quality = $MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer/PlayerQuality
onready var competitor_quality = $MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer2/CompetitorQuality
onready var competitor_texture = $MarginContainer/VBoxContainer/HBoxContainer/HBoxContainer2/TextureRect
onready var suspicion = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/Suspicion
onready var time_hand = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer2/HBoxContainer/ClockFace/ClockHand
onready var countdown = $AudioStreamPlayer

signal timeout

func _ready():
	player_quality.value = 100
	competitor_quality.value = 100
	set_process(true)

func attachStadium(get_stadium):
	stadium = get_stadium
	connect("timeout", stadium, "finishRound")
	var face_frame = stadium.get_node("HBoxContainer/Node2D2/Competitor").get_node("Body/Head").frame
	var competitor_faces = stadium.get_node("HBoxContainer/Node2D2/Competitor").get_node("Body/Head").texture.get_data()
	var face_image = competitor_faces.get_rect(Rect2(16 * face_frame % 2, 18 * face_frame / 2, 16, 18))
	var competitor_image = ImageTexture.new()
	competitor_image.create_from_image(face_image, 2)
	competitor_texture.set_texture(competitor_image)

func _process(delta):
	time_left -= delta
	time_hand.set_rotation(deg2rad(360 * time_left / total_time))
	if (time_left <= 0):
		emit_signal("timeout")
	if (player_quality.value <= 0 or competitor_quality.value <= 0):
		emit_signal("timeout")
	if (time_left < 10):
		if(!countdown.is_playing()):
			countdown.play()
	if (suspicion.value >= 100):
		Main.setCheated(true)
		emit_signal("timeout")
	if (competitor_quality.value < 25):
		competitor_quality.get_stylebox("fg").set_bg_color(Color(1.0, 0.0, 0.0))
		competitor_quality.get_node("AnimationPlayer").play("Blink")
	if (player_quality.value < 25):
		player_quality.get_stylebox("fg").set_bg_color(Color(1.0, 0.0, 0.0))
		player_quality.get_node("AnimationPlayer2").play("Blink")

func setLevel(get_level):
	level = get_level
	total_time = level.getTotalTime()
	time_left = total_time

func increaseSuspicion():
	suspicion.value += level.getSuspicionDelta()

func getCompetitorQuality():
	return competitor_quality.value

func setCompetitorQuality(quality):
	competitor_quality.value = quality

func getPlayerQuality():
	return player_quality.value

func setPlayerQuality(quality):
	player_quality.value = quality

func determineWinner():
	return true if player_quality.value > competitor_quality.value else false