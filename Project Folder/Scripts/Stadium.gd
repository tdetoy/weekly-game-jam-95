extends Node2D

var level
var particles = false
var havoc = false
var duration = 0.0
var increase = 0.0
var sharpening = false
var delay = false
var distract_timer = 0.0
var announced_self = false

var player_pot_degrade = 0.0
var player_pan_degrade = 0.0

var competitor_pot_degrade = 0.0
var competitor_pan_degrade = 0.0

onready var player_pot = $HBoxContainer/Node2D/Sprite/HBoxContainer/Pot
onready var competitor_pot = $HBoxContainer/Node2D2/Sprite/HBoxContainer/Pot
onready var player_pan = $HBoxContainer/Node2D/Sprite/HBoxContainer/Pan
onready var competitor_pan = $HBoxContainer/Node2D2/Sprite/HBoxContainer/Pan
onready var player = $HBoxContainer/Node2D/Player
onready var judge = $HBoxContainer/Judge
onready var competitor = $HBoxContainer/Node2D2/Competitor
onready var player_cut = $HBoxContainer/Node2D/Sprite/HBoxContainer/Cut
onready var competitor_cut = $HBoxContainer/Node2D2/Sprite/HBoxContainer/Cut
onready var notice = $HBoxContainer/Judge/Notice
onready var distraction = $HBoxContainer/Judge/Distraction

onready var ui = $UI

signal casting
signal cast_cancel
signal cut
signal change_temp
signal release
signal toggle_lid
signal stirring
signal stir_end
signal sharpen
signal left
signal right
signal distract_start(type)

func _ready():
	connect("cut", player_cut, "cutAction")
	connect("cut", player, "cutAction")
	connect("casting", self, "castStart")
	connect("cast_cancel", self, "castStop")
	connect("cast_cancel", player, "endCast")
	connect("casting", player, "startCast")
	connect("casting", judge, "enableDanger")
	connect("cast_cancel", judge, "disableDanger")
	connect("change_temp", player_pan, "changeBurner")
	connect("release", player_pan, "stopAdjust")
	connect("toggle_lid", player_pot, "toggleLid")
	connect("stirring", player_pot, "stir")
	connect("stir_end", player_pot, "stopStir")
#	connect("stirring", player, "stir")
#	connect("stir_end", player, "stopStir")
	connect("change_temp", player, "changeBurner")
	connect("release", player, "stopAdjust")
	connect("sharpen", player_cut, "sharpen")
	connect("left", player, "moveLeft")
	connect("right", player, "moveRight")
	connect("distract_start", distraction, "createDistraction")
	
#	levelAssign(load("res://Scripts/Level1.gd").new())
	attachSelf()
	set_process(true)
	competitor.setHead(Main.getCompetitorHead())
	competitor_pan.debugGetOwner(competitor)
	player_pan.debugGetOwner(player)
	Main.getStats().setPlayer(player)
	Main.getStats().setCompetitor(competitor)
	Main.setCheated(false)

func _unhandled_input(event):
	if (event.is_action_pressed("ui_select") and !delay):
		emit_signal("casting")
		delayForAction()
	elif (event.is_action_released("ui_select")):
		emit_signal("cast_cancel")
		finishDelay()
	elif (event.is_action_pressed("ui_left") and !delay):
		emit_signal("left")
	elif (event.is_action_pressed("ui_right") and !delay):
		emit_signal("right")
	elif (event.is_action_pressed("ui_up")):
		if (!delay):
			match player.getStationPos():
				0:
					emit_signal("sharpen")
					delayForAction()
				2:
					emit_signal("toggle_lid")
		if (delay):
			match player.getStationPos():
				1:
					emit_signal("release")
	elif (event.is_action_pressed("ui_down")):
		if (!delay):
			match player.getStationPos():
				0:
					emit_signal("cut")
				1:
					emit_signal("change_temp")
					delayForAction()
				2:
					emit_signal("stirring")
					delayForAction()
	elif (event.is_action_released("ui_down")):
		match player.getStationPos():
			2:
				emit_signal("stir_end")
		pass


func _process(delta):
	if (!announced_self):
		print("made it here")
		announced_self = true
	if (!distraction.isDistracting()):
		distract_timer -= delta
	if(distract_timer <= 0 and !distraction.isDistracting()):
		spawnDistraction()
	if(havoc):
		if(!particles):
			competitor_pot.startParticles()
			competitor_pan.startParticles()
			competitor_cut.startParticles()
			particles = true
		competitor_pot.increaseMultiplier(increase)
		competitor_pan.increaseMultiplier(increase)
		competitor_cut.increaseMultiplier(increase)
		duration += delta
		increase = level.getHavocMult() * pow(1, duration * level.getHavocMult())
		print(increase)
	if(!havoc and particles):
		competitor_pot.stopParticles()
		competitor_pan.stopParticles()
		competitor_cut.stopParticles()
		particles = false
		

func levelAssign(get_level):
	level = get_level
	level.createGradient()
	$HBoxContainer/Node2D.setLevel(level)
	$HBoxContainer/Node2D2.setLevel(level)
	competitor.setLevel(level)
	$HBoxContainer/Judge.setLevel(level)
	ui.setLevel(level)
	distract_timer = randi() % level.getDistractCooldown() + level.getDistractCooldown()

func castStart():
	havoc = true

func castStop():
	havoc = false
	duration = 0.0
	increase = 0.0
	$HBoxContainer/Node2D2/Sprite/HBoxContainer/Pot.resetMultiplier()
	$HBoxContainer/Node2D2/Sprite/HBoxContainer/Pan.resetMultiplier()
	$HBoxContainer/Node2D2/Sprite/HBoxContainer/Cut.resetMultiplier()

func lowerQuality(obj):
	if (obj == player_pot or obj == player_pan):
		if (obj == player_pot):
			player_pot_degrade += level.getPlayerDegrade()
		elif (obj == player_pan):
			player_pan_degrade += level.getPlayerDegrade()
		ui.setPlayerQuality(ui.getPlayerQuality() - level.getPlayerDegrade())
	elif (obj == competitor_pot or obj == competitor_pan):
		if (obj == competitor_pot):
			competitor_pot_degrade += level.getCompetitorDegrade()
		elif (obj == competitor_pan):
			competitor_pan_degrade += level.getCompetitorDegrade()
		ui.setCompetitorQuality(ui.getCompetitorQuality() - level.getCompetitorDegrade())

func attachSelf():
	player.attachStadium(self)
	competitor.attachStadium(self)
	player_pot.attachStadium(self)
	competitor_pot.attachStadium(self)
	player_pan.attachStadium(self)
	competitor_pan.attachStadium(self)
	player_cut.attachStadium(self)
	competitor_cut.attachStadium(self)
	judge.attachStadium(self)
	distraction.attachStadium(self)
	ui.attachStadium(self)

func increaseSuspicion():
	ui.increaseSuspicion()
	notice.visible = true

func safeAgain():
	notice.visible = false
	

func doneSharpening():
	sharpening = false

func getDest(actor, location):
	if (actor == player):
		match location:
			'pot':
				return player_pot.position.x
			'pan':
				return player_pan.position.x
			'cut':
				return player_cut.position.x
	if (actor == competitor):
		match location:
			'pot':
				return competitor_pot.position.x
			'pan':
				return competitor_pan.position.x
			'cut':
				return competitor_cut.position.x

func delayForAction():
	delay = true

func finishDelay():
	delay = false

func spawnDistraction():
	randomize()
	if (randi() % 2 == 0):
		emit_signal("distract_start", "talking")
		distraction.position.x = 200
	else:
		emit_signal("distract_start", "reading")
		distraction.position.x = -200

func getJudge():
	return judge

func getOwner(item):
	if (item == competitor_pot or item == competitor_pan or item == competitor_cut):
		return competitor
	elif (item == player_pot or item == player_pan or item == player_cut):
		return player

func resetDistraction():
	distract_timer = randi() % level.getDistractCooldown() + level.getDistractCooldown()

func getCompetitorPot():
	return competitor_pot

func getCompetitorPan():
	return competitor_pan

func getCompetitorCut():
	return competitor_cut

func getDistraction():
	return distraction

func finishRound():
	if(Main.getCheated()):
		Main.getStats().setWinner(competitor) 
		competitor.setWin()
	else:
		ui.setPlayerQuality(ui.getPlayerQuality() - (level.getPlayerDegrade() * player_cut.getRemainingItems() * 10))
		ui.setCompetitorQuality(ui.getCompetitorQuality() - (level.getCompetitorDegrade() * competitor_cut.getRemainingItems() * 10))
		Main.getStats().setWinner(player) if ui.determineWinner() else Main.getStats().setWinner(competitor)
		player.setWin() if Main.getStats().playerWon() else competitor.setWin()
	Main.getStats().setPlayerStations(player_cut.getRemainingItems() * level.getPlayerDegrade() * 10, player_pan_degrade, player_pot_degrade)
	Main.getStats().setCompetitorStations(competitor_cut.getRemainingItems() * level.getCompetitorDegrade() * 10, competitor_pan_degrade, competitor_pot_degrade)
	Main.transition("post", self)