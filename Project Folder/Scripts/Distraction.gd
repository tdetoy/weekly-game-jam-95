extends Node2D

onready var appearance = $Sprite
onready var anim = $Sprite/AnimationPlayer

var moving_left = false
var moving_right = false

var dist_type
var stadium
var judge
var distraction_done = false
var distracting = false

signal distract(type) 
signal distraction_done

func _ready():
	connect("distract", self, "executeDistraction")
	set_process(true)

func _process(delta):
	if (distracting):
		if (anim.get_current_animation() != "DistractionTalking" and anim.get_current_animation() != "DistractionPaper"):
			if (!distraction_done):
				if (moving_left == true):
					position.x -= delta * 10
					print(position.x)
					if (position.x <= 25):
						emit_signal("distract", "talking")
				elif (moving_right == true):
					position.x += delta * 10
					print(position.x)
					if (position.x >= -25):
						emit_signal("distract", "reading")
			#if done, head the other way
			if (distraction_done):
				if (moving_left == true):
					position.x += delta * 10
					if (position.x >= 250):
						emit_signal("distraction_done")
						distracting = false
				elif (moving_right == true):
					position.x -= delta * 10
					if (position.x <= -250):
						emit_signal("distraction_done")
						distracting = false

func attachStadium(scene):
	stadium = scene
	judge = stadium.getJudge()
	connect("distraction_done", stadium, "resetDistraction")
	connect("distract", judge, "getDistracted")

func createDistraction(type):
	dist_type = type
	distracting = true
	print("distracting. type: " + type)
	if (type == "talking"):
		appearance.scale = Vector2(-1, 1)
		moving_left = true
	else:
		appearance.scale = Vector2(1, 1)
		moving_right = true
	anim.play("DistractionWalking")

func finishDistraction():
	anim.stop()
	appearance.scale.x *= -1
	distraction_done = true

func executeDistraction(type):
	if (type == "talking"):
		anim.play("DistractionTalking")
	elif (type == "reading"):
		anim.play("DistractionPaper")
		finishDistraction()

func isDistracting():
	return distracting