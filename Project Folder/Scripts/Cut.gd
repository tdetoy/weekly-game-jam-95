extends Node2D

onready var cut_meter = $VBoxContainer/ItemChopped
onready var items_left = $VBoxContainer/ItemProgress
onready var knife_status = $VBoxContainer/KnifeQuality

var level
var num_items = 0
var casting_multiplier = 1.0
var stadium
var sharpening = false
var sharpening_gauge = 0.0

signal finish

func _ready():
	items_left.value = 0
	cut_meter.value = 0
	knife_status.value = 100
	set_process(true)

func _process(delta):
	if (sharpening):
		sharpening_gauge += delta * level.getSharpeningValue()
		knife_status.value = floor(sharpening_gauge)
		if (sharpening_gauge >= 100):
			sharpening = false
			emit_signal("finish")

func cutAction():
	cut_meter.value += level.getCutValue() * ((knife_status.value + 1) / 100)
	knife_status.value -= level.getKnifeDegrade()
	if (cut_meter.value >= 100):
		cut_meter.value = 0
		num_items += 1
		items_left.value = floor((float(num_items) / float(level.getNumItems())) * 100)
		print(items_left.value)
		print(floor((float(num_items) / float(level.getNumItems())) * 100))

func sharpen():
	sharpening = true
	sharpening_gauge = 0.0
	knife_status.value = 0

func setLevel(get_level):
	level = get_level

func increaseMultiplier(value):
	casting_multiplier = value

func resetMultiplier():
	casting_multiplier = 1.0

func attachStadium(get_stadium):
	stadium = get_stadium
	connect("finish", stadium, "finishDelay")
	print(stadium.getOwner(self).name)
	if (stadium.getOwner(self).name == "Competitor"):
		connect("finish", stadium.getOwner(self), "finishDelay")

func getStatus():
	return knife_status.value

func getRemainingItems():
	var val = level.getNumItems() - num_items
	if val < 0:
		return 0
	return val
	
func startParticles():
	$CPUParticles2D.set_emitting(true)

func stopParticles():
	$CPUParticles2D.set_emitting(false)