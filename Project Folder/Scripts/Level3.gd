extends Resource

var difficulty = 6
var pot_multi = 1.25
var pot_difficulty = 1.25
var pan_band = 0.3
var knife_degrade = 4
var sharpening_value = 30
var num_items = 8
var cut_value = 15
var player_degrade = .03
var competitor_degrade = .05
var pan_reset = 7
var pan_value = 1
var pan_difficulty = 15
var pan_temp_change = 4
var wander = 1
var total_time = 45
var suspicion_delta = 1.25
var havoc_mult = 8
var distract_chance = 0.4
var distract_cooldown = 8
var lower_bound
var upper_bound
var new_gradient = Gradient.new()
var competitor_forgetfulness = 35
var competitor_delay = 4.0

func getDifficulty():
	return difficulty

func getPanBand():
	return pan_band

func getPotDifficulty():
	return pot_difficulty

func getPotMult():
	return pot_multi

func getCutValue():
	return cut_value

func getKnifeDegrade():
	return knife_degrade

func getPlayerDegrade():
	return player_degrade

func getCompetitorDegrade():
	return competitor_degrade

func getPanReset():
	return pan_reset

func getPanValue():
	return pan_value

func getPanDifficulty():
	return pan_difficulty

func getJudgeWander():
	return wander

func getTotalTime():
	return total_time

func getSuspicionDelta():
	return suspicion_delta

func getHavocMult():
	return havoc_mult

func getNumItems():
	return num_items

func getSharpeningValue():
	return sharpening_value

func getDistractChance():
	return distract_chance

func getDistractCooldown():
	return distract_cooldown

func getJudgeDistraction():
	pass

func getPanTempChange():
	return pan_temp_change

func createGradient():
	var color_array = PoolColorArray()
	
	color_array.append(Color.red)
	color_array.append(Color.red)
	color_array.append(Color.green)
	color_array.append(Color.green)
	color_array.append(Color.red)
	color_array.append(Color.red)
	
	new_gradient.set_colors(color_array)

	randomize()
	lower_bound = max(randf(), 0.0)
	lower_bound = min(lower_bound, 1.0 - getPanBand())
	upper_bound = min(lower_bound + getPanBand(), 1.0)
	
	var return_val = PoolRealArray()
	return_val.append(0)
	return_val.append(lower_bound - 0.01)
	return_val.append(lower_bound)
	return_val.append(upper_bound)
	return_val.append(upper_bound + 0.01)
	return_val.append(1.0)
	
	new_gradient.set_offsets(return_val)

func getGradient():
	return new_gradient

func getLowerBound():
	return lower_bound

func getUpperBound():
	return upper_bound

func getComptetitorForgetfulness():
	return competitor_forgetfulness

func getCompetitorDelay():
	return competitor_delay

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
