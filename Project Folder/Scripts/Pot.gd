extends Node2D

onready var appearance = $PotSprite
onready var status = $ProgressBar
onready var anim = $PotSprite/AnimationPlayer
onready var noise = $PotSprite/AudioStreamPlayer
onready var wiggle_anim = $PotSprite/AnimationPlayer2

var level
var stadium
var lid = true
var stirring = false
var pot_value = 5
var casting_multiplier = 1
var known = false

signal finish
signal stir
signal stop_stir

func _ready():
	status.value = 100
	appearance.frame = 0
	set_process(true)

func _process(delta):
	if (!stirring):
		status.value -= casting_multiplier * delta * level.getPotDifficulty()
	
	if (stirring):
		status.value += delta * 5
	
	if (!lid):
		status.value -= 2 * casting_multiplier * delta * level.getPotDifficulty()
	
	if (status.value <= 0):
		if (!known):
			known = true
			noise.play()
#		print("your pot sucks")
		stadium.lowerQuality(self)
		if (!wiggle_anim.is_playing()):
			anim.play("Wiggle")

func setLevel(get_level):
	level = get_level
	pot_value *= level.getPotMult()

func removeLid():
	appearance.frame = 2
	anim.play("Boiling")
	lid = false

func replaceLid():
	anim.stop()
	appearance.frame = 0
	lid = true

func attachStadium(scene):
	stadium = scene
	connect("finish", stadium, "finishDelay")
	connect("stir", stadium.getOwner(self), "stir")
	connect("stop_stir", stadium.getOwner(self), "stopStir")

func increaseMultiplier(value):
	casting_multiplier = value

func resetMultiplier():
	casting_multiplier = 1.0

func stir():
	if(!lid):
		stirring = true
		emit_signal("stir")
		known = false
		wiggle_anim.stop()
		appearance.set_offset(Vector2(0.5,0))

func stopStir():
	stirring = false
	emit_signal("finish")
	emit_signal("stop_stir")

func toggleLid():
	if (!lid):
		replaceLid()
	elif (lid):
		removeLid()

func hasLid():
	return lid

func getStatus():
	return status.value

func startParticles():
	$PotSprite/CPUParticles2D.set_emitting(true)

func stopParticles():
	$PotSprite/CPUParticles2D.set_emitting(false)

