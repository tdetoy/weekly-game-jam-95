extends Control

onready var judge = $MarginContainer/VBoxContainer/HBoxContainer2/Judge
onready var loser = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/LoserPosition
onready var winner = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/WinnerPosition
onready var win_pot = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer2/Winner
onready var lose_pot = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer2/Loser
onready var win_pan = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer/Winner
onready var lose_pan = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer/Loser
onready var win_cut = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer3/Winner
onready var lose_cut = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/HBoxContainer3/Loser
onready var judge_input = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/JudgeInput

var stats


func _ready():
	judge.set_process(false)

func buildScreen(get_stats):
	get_tree().paused = true
	stats = get_stats
	var loser_node = stats.getLoser().duplicate()
	var winner_node = stats.getWinner().duplicate()
	loser.add_child(loser_node)
	winner.add_child(winner_node)
	winner_node.position = Vector2(65,120)
	winner_node.scale = Vector2(4, 4)
	loser_node.position = Vector2(65, 120)
	loser_node.scale = Vector2(4, 4)
	win_pot.text = str(stats.getPot("winner"))
	lose_pot.text = str(stats.getPot("loser"))
	win_pan.text = str(stats.getPan("winner"))
	lose_pan.text = str(stats.getPan("loser"))
	win_cut.text = str(stats.getCut("winner"))
	lose_cut.text = str(stats.getCut("loser"))
	determineJudgeInput()

func determineJudgeInput():
	var feedback = ""
	randomize()
	if stats.playerWon():
		var string = randi() % 3
		match string:
			0:
				feedback += "Well done, or something!\n "
			1:
				feedback += "You did it, apparently!\n "
			2:
				feedback += "Congratulations, I suppose!\n "
		if (stats.getPot("winner") > stats.getPan("winner") and stats.getPot("winner") > stats.getCut("winner")):
			feedback += "Watch that pot, though! You really let the contents (whatever they are) stick together too much!\n"
		elif (stats.getPan("winner") > stats.getPot("winner") and stats.getPan("winner") > stats.getCut("winner")):
			feedback += "Pay attention to the temperature of your pan, though! It was a mess!\n"
		elif (stats.getCut("winner") > stats.getPot("winner") and stats.getPan("winner") < stats.getCut("winner")):
			feedback += "You definitely have to work on your cutting skills though!\n"
		else:
			feedback += "I feel like you could have changed... something though! I dunno!\n"
		feedback += " I apologize (but not really), I'm medically incapable of giving a compliment without a caveat!"
	else:
		var string = randi() % 3
		match string:
			0:
				feedback += "Oof, ow, bad luck.\n "
			1:
				feedback += "Disappointed, but not surprised I guess.\n "
			2:
				feedback += "I love this part- being brutally honest. You lost.\n "
		if (Main.getCheated()):
			feedback += "That's what you get for cheating though! I don't even know what you were doing,\n but I'm pretty sure hand wiggles and chanting isn't cooking, or allowed!\n"
		elif (stats.getPot("loser") > stats.getPan("loser") and stats.getPot("loser") > stats.getCut("loser")):
			feedback += "I'm not entirely sure you know how to boil things properly. Did you stir it at all?\n"
		elif (stats.getPan("loser") > stats.getPot("loser") and stats.getPan("loser") > stats.getCut("loser")):
			feedback += "Your meal was cooked... well, was it even? I can't tell if it was overcooked or undercooked, but it sure wasn't right!\n"
		elif (stats.getCut("loser") > stats.getPot("loser") and stats.getPan("loser") < stats.getCut("loser")):
			feedback += "None of your... whatever those are... were cut! I had to eat one like an apple! It was weird!\n"
		else:
			feedback += "I'm not even sure where to begin!\n"
		feedback += " Ahh. That feels better. I do love a good rant."
	judge_input.text = feedback


func _on_MainMenu_pressed():
	Main.transition("main_menu", self)
