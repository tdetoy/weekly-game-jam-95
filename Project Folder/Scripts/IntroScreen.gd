extends Control

onready var comp_appearance = $MarginContainer/HBoxContainer/Competitor2
onready var title = $MarginContainer/HBoxContainer/VBoxContainer/Title
onready var infodump = $MarginContainer/HBoxContainer/VBoxContainer/Info

var difficulty

# Called when the node enters the scene tree for the first time.
func _ready():
	populateValues()

func populateValues():
	randomize()
	var head = randi() % 6
	Main.setCompetitorHead(head)
	comp_appearance.setHead(head)
	comp_appearance.tPose()
	setTitleText()
	setInfodumpText()

func setTitleText():
	match Main.getLevel():
		1:
			title.text = "Qualifiers"
		2:
			title.text = "First Round"
		3:
			title.text = "Quarterfinals"
		4:
			title.text = "Semifinals"
		5:
			title.text = "Final Round"

func _on_Button_pressed():
	Main.transition("stadium", self)

func setInfodumpText():
	randomize()
	var result = randi() % 5
	match result:
		0:
			infodump.text = "Hi there! I'm in this competition because I want to\nhonor my grandmother who recently passed.\nShe taught me everything I know and I want to show the world\nthat she was worthwhile. It would mean so much to me\nif I won, you wouldn't even know. *sniff*\n I still get choked up thinking about it a little."
		1:
			infodump.text = "Greetings. I've spent my whole life cooking because\nit gives me meaning. I want to win this to\nprove that I have not wasted my time focusing on the culinary\narts, and that all the sacrifice was worth it.\nSo much sacrifice..."
		2:
			infodump.text = "Hello.\n I'm looking to win the prize money to pay\nfor some very expensive operations for my\n dog. I simply don't know what I'd do without him and the prize money\n would go a long way towards offsetting\nthe surgery costs. I hope I win..."
		3:
			infodump.text = "Good day to you.\nI'm the chef at a struggling restaurant that\ncontinually gets good reviews from food critics.\nI'm hoping winning this competition will provide some\nmuch-needed publicity soon, so that we don't go under.\n I wish you the best of luck as well."
		4:
			infodump.text = "Oh man I don't even know what I'm doing here...\n the girl I love said she'd say yes to marrying me if I won\nand I'm so nervous... I don't even know how I got this far,\nbut it must be love guiding me! Truly serendipity is\non my side!"