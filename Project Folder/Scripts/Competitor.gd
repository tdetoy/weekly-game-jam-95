extends Node2D

onready var head_anim = $Body/Head/AnimationPlayer
onready var body_anim = $Body/AnimationPlayer
onready var body = $Body
onready var head = $Body/Head

var delay = false
var station_pos = 1
var stadium
var pot
var pan
var cut
var target
var level
var delay_timer

func _ready():
	set_process(true)

func _process(delta):
	if (delay_timer > 0.0 and !body_anim.is_playing()):
		delay_timer -= delta
	if (!body_anim.is_playing() and !delay and delay_timer <= 0.0):
		if (station_pos == 0):
			body.frame = 5
		else:
			body.frame = 0
		target = determineTarget()
		if (target > station_pos):
			moveRight()
			delay_timer = level.getCompetitorDelay() / 2
		elif (target < station_pos):
			moveLeft()
			delay_timer = level.getCompetitorDelay() / 2
		elif (target == station_pos):
			match station_pos:
				0:
					cutAction()
					delay_timer = level.getCompetitorDelay() / 10
				1:
					changeBurner()
					delay_timer = level.getCompetitorDelay()
				2:
					checkLid()
					delay_timer = level.getCompetitorDelay()
	elif (body_anim.get_current_animation() == "Oil"):
		if (pan.getKnobStatus()):
			stopAdjust()
	elif (body_anim.get_current_animation() == "Stir"):
		if (pot.getStatus() > 90):
			stopStir()

func setWin():
	body.frame = 1

func moveLeft():
	match station_pos:
		1:
			station_pos = 0
			self.position.x = stadium.getDest(self,'cut')
		2:
			station_pos = 1
			self.position.x = stadium.getDest(self,'pan')
		_:
			print("oops")

func moveRight():
	match station_pos:
		0:
			station_pos = 1
			self.position.x = stadium.getDest(self, 'pan')
		1:
			station_pos = 2
			self.position.x = stadium.getDest(self, 'pot')
		_:
			print("oops")

func getStationPos():
	return station_pos

func stir():
	body_anim.play("Stir")

func stopStir():
	body_anim.stop()
	pot.stopStir()
	if (randi() % 100 < level.getCompetitorForgetfulness()):
		pot.toggleLid()

func cutAction():
	if (cut.getStatus() < 10):
		sharpen()
	else:
		body_anim.play("Cut")
		cut.cutAction()

func sharpen():
	cut.sharpen()
	delayForAction()

func changeBurner():
	body_anim.play("Oil")
	pan.changeBurner()

func stopAdjust():
	body_anim.stop()
	pan.stopAdjust()

func attachStadium(scene):
	stadium = scene
	pot = stadium.getCompetitorPot()
	pan = stadium.getCompetitorPan()
	cut = stadium.getCompetitorCut()

func setLevel(res):
	level = res
	delay_timer = level.getCompetitorDelay() / 2

func delayForAction():
	delay = true

func finishDelay():
	delay = false

func determineTarget():
	if (pot.getStatus() < 10):
		return 2
	elif (!pan.getTempStatus()):
		return 1
	elif (!pot.hasLid()):
		return 2
	else:
		return 0

func checkLid():
	if (pot.hasLid()):
		pot.toggleLid()
		pot.stir()
		stir()
	elif (!pot.hasLid()):
		pot.toggleLid()

func setHead(num):
	head.frame = num