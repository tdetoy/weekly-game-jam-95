extends Node2D

onready var level1 = load("res://Scripts/Level1.gd").new()
onready var level2 = load("res://Scripts/Level2.gd").new()
onready var level3 = load("res://Scripts/Level3.gd").new()
onready var level4 = load("res://Scripts/Level4.gd").new()
onready var level5 = load("res://Scripts/Level5.gd").new()
onready var post = load("res://Scenes/PostGame.tscn").instance()
onready var stadium = load("res://Scenes/Stadium.tscn").instance()
onready var instructions = load("res://Scenes/Instructions.tscn").instance()
onready var instructions2 = load("res://Scenes/Instructions2.tscn").instance()
onready var title = load("res://Scenes/Title.tscn").instance()
onready var intro = load("res://Scenes/IntroScreen.tscn").instance()
onready var credits = load("res://Scenes/Credits.tscn").instance()
onready var music = $AudioStreamPlayer
onready var menu_music = load("res://Sounds/MainMenu.wav")
onready var stadium_music = load("res://Sounds/SaneLevel.wav")
onready var final_level_music = load("res://Sounds/CrazyLevel.wav")
onready var introduction_music = load("res://Sounds/Juhani Junkala [Retro Game Music Pack] Level 2.wav")
onready var instructions_music = load("res://Sounds/Juhani Junkala [Retro Game Music Pack] Ending.wav")

var stats
var level
var competitor_head
var level_num
var audio = true
var cheated = false

class Stats:
	var player
	var competitor
	var player_cut
	var player_pan
	var player_pot
	var competitor_cut
	var competitor_pan
	var competitor_pot
	var player_won = false
	
	func setPlayer(node):
		player = node
	
	func setCompetitor(node):
		competitor = node
	
	func setCompetitorStations(cut, pan, pot):
		competitor_cut = cut
		competitor_pan = pan
		competitor_pot = pot
	
	func setPlayerStations(cut, pan, pot):
		player_cut = cut
		player_pan = pan
		player_pot = pot
	
	func setWinner(node):
		if (node == player):
			player_won = true
		elif (node == competitor):
			player_won = false
	
	func getWinner():
		return player if player_won else competitor
	
	func getLoser():
		return competitor if player_won else player

	func getCut(position):
		if (position == "winner"):
			return player_cut if player_won else competitor_cut
		elif (position == "loser"):
			return competitor_cut if player_won else player_cut

	func getPan(position):
		if (position == "winner"):
			return player_pan if player_won else competitor_pan
		elif (position == "loser"):
			return competitor_pan if player_won else player_pan

	func getPot(position):
		if (position == "winner"):
			return player_pot if player_won else competitor_pot
		elif (position == "loser"):
			return competitor_pot if player_won else player_pot
	
	func playerWon():
		return player_won

func _ready():
	stats = Stats.new()
	music.play()
	set_process(true)

func _process(delta):
	if (!music.is_playing() and audio):
		music.play()

func getStats():
	return stats

func setLevel(value):
	match value:
		1:
			level_num = 1
			level = level1
		2:
			level_num = 2
			level = level2
		3:
			level_num = 3
			level = level3
		4:
			level_num = 4
			level = level4
		5:
			level_num = 5
			level = level5

func clearCache():
	level1 = load("res://Scripts/Level1.gd").new()
	post = load("res://Scenes/PostGame.tscn").instance()
	stadium = load("res://Scenes/Stadium.tscn").instance()
	instructions = load("res://Scenes/Instructions.tscn").instance()
	instructions2 = load("res://Scenes/Instructions2.tscn").instance()
	title = load("res://Scenes/Title.tscn").instance()
	intro = load("res://Scenes/IntroScreen.tscn").instance()

func transition(next, current):
	if next == "post":
		get_tree().get_root().add_child(post)
		post.buildScreen(stats)
		music.set_stream(instructions_music)
		music.play()
	if next == "stadium":
		get_tree().get_root().add_child(stadium)
		stadium.levelAssign(level)
		music.set_stream(stadium_music)
		if(level_num == 5):
			music.set_stream(final_level_music)
		music.play()
	if next == "instructions":
		get_tree().get_root().add_child(instructions)
		music.set_stream(instructions_music)
		music.play()
	if next == "instructions2":
		get_tree().get_root().add_child(instructions2)
	if next == "main_menu":
		get_tree().get_root().add_child(title)
		music.set_stream(menu_music)
		music.play()
	if next == "intro":
		get_tree().get_root().add_child(intro)
		music.set_stream(introduction_music)
		music.play()
	if next == "credits":
		get_tree().get_root().add_child(credits)
	current.queue_free()
	clearCache()

func getCompetitorHead():
	return competitor_head

func setCompetitorHead(num):
	competitor_head = num

func getLevel():
	return level_num

func setAudio(state):
	audio = state
	if(!audio):
		music.set_volume_db(-10000)
	if(audio):
		music.set_volume_db(-10)

func getAudio():
	return audio

func setCheated(state):
	cheated = state

func getCheated():
	return cheated