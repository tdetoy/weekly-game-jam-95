extends Control

var difficulty

onready var qualifier_button = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/Qualifiers
onready var round_one_button = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer2/RoundOne
onready var qfinals_button = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer3/QuarterFinals
onready var sfinals_button = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer4/SemiFinals
onready var finalrd_button = $MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer5/FinalRound

func _ready():
	if (!Main.getAudio()):
		$MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer2/Mute.set_toggle_mode(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Start_pressed():
	if (!difficulty):
		var window = AcceptDialog.new()
		add_child(window)
		window.set_text("Select a Difficulty!")
		window.popup_centered(Vector2(250, 25))
	else:
		Main.setLevel(difficulty)
		Main.transition("intro", self)


func _on_Instructions_pressed():
	Main.transition("instructions",self)


func _on_Qualifiers_toggled(button_pressed):
	untoggleAll()
	difficulty = 1


func _on_RoundOne_toggled(button_pressed):
	untoggleAll()
	difficulty = 2


func _on_QuarterFinals_toggled(button_pressed):
	untoggleAll()
	difficulty = 3


func _on_SemiFinals_toggled(button_pressed):
	untoggleAll()
	difficulty = 4


func _on_FinalRound_toggled(button_pressed):
	untoggleAll()
	difficulty = 5

func untoggleAll():
	if qualifier_button.is_pressed():
		qualifier_button.set_pressed(false)
	if round_one_button.is_pressed():
		round_one_button.set_pressed(false)
	if qfinals_button.is_pressed():
		qfinals_button.set_pressed(false)
	if sfinals_button.is_pressed():
		sfinals_button.set_pressed(false)
	if finalrd_button.is_pressed():
		finalrd_button.set_pressed(false)



func _on_Mute_toggled(button_state):
	Main.setAudio(!Main.getAudio())


func _on_Credits_pressed():
	Main.transition("credits",self)
