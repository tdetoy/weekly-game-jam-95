extends Node2D

onready var head_anim = $Body/Head/AnimationPlayer
onready var body_anim = $Body/AnimationPlayer
onready var body = $Body

var station_pos = 1
var stadium


func _ready():
	set_process(true)

func _process(delta):
	if (!body_anim.is_playing()):
		if (station_pos == 0):
			body.frame = 5
		else:
			body.frame = 0

func startCast():
	head_anim.play("HeadCast")
	body_anim.play("Cast")

func endCast():
	head_anim.play("HeadBob")
	body_anim.stop()
	body.frame = 0

func attachStadium(get_station):
	stadium = get_station

func setWin():
	body.frame = 1


func moveLeft():
	match station_pos:
		1:
			station_pos = 0
			self.position.x = stadium.getDest(self,'cut')
		2:
			station_pos = 1
			self.position.x = stadium.getDest(self,'pan')
		_:
			print("oops")

func moveRight():
	match station_pos:
		0:
			station_pos = 1
			self.position.x = stadium.getDest(self, 'pan')
		1:
			station_pos = 2
			self.position.x = stadium.getDest(self, 'pot')
		_:
			print("oops")

func getStationPos():
	return station_pos

func stir():
	body_anim.play("Stir")

func stopStir():
	body_anim.stop()

func cutAction():
	body_anim.play("Cut")

func changeBurner():
	body_anim.play("Oil")

func stopAdjust():
	body_anim.stop()