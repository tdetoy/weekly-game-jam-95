extends Node2D

onready var appearance = $PanSprite
onready var temp = $HeatSlider
onready var temp_gauge = $HeatSlider2
onready var anim = $PanSprite/AnimationPlayer
onready var noise = $PanSprite/AudioStreamPlayer
onready var wiggle_anim = $PanSprite/AnimationPlayer2

var level
var stadium
var temp_float
var pan_value = 5
var increasing = true
var reset_timer
var fixing = false
var adjust_increase = true
var casting_multiplier = 1.0
#var color_array = PoolColorArray()
var lower_bound
var upper_bound
var adjust_value = 0.0
var chef
var temp_target
var temp_target_float
var known = false

signal finish

func _ready():
#	color_array.append(Color.red)
#	color_array.append(Color.red)
#	color_array.append(Color.green)
#	color_array.append(Color.green)
#	color_array.append(Color.red)
#	color_array.append(Color.red)
	
	temp.value = 50
	temp_float = temp.value
	set_process(true)
	print(temp.get_stylebox("slider").get_texture().get_gradient().get_offsets())
	var view_array = temp.get_stylebox("slider").get_texture().get_gradient().get_colors()


func _process(delta):
	reset_timer -= delta * casting_multiplier
	
	if (reset_timer <= 0 and !fixing):
		scrambleBurner()
	
	checkAnim()
	
	var direction
	increasing = true if temp.value < temp_gauge.value else false
	
	var gauge_direction
	var gauge_increasing
	gauge_increasing = true if temp_gauge.value < temp_target else false
	
	if (!increasing):
		direction = -1
	else:
		direction = 1
	
	if (!gauge_increasing):
		gauge_direction = -1
	else:
		gauge_direction = 1
	
	
	var change = 2 * direction * casting_multiplier * delta * level.getPanTempChange()
	var gauge_change = 2 * gauge_direction * casting_multiplier * delta * level.getPanDifficulty()
	
	if (temp.value != temp_gauge.value and !fixing):
		temp_float += change
		temp_float = clamp(temp_float, 0, 100)
		temp.value = floor(temp_float)
	if (temp_gauge.value != temp_target and !fixing):
		temp_target_float += gauge_change
		temp_target_float = clamp(temp_target_float, 0, 100)
		temp_gauge.value = floor(temp_target_float)
		
	elif (fixing and adjust_increase):
		change = 3 * delta * level.getPanDifficulty()
		adjust_value += change
		adjust_value = clamp(adjust_value, 0, 100)
		temp_gauge.value = floor(adjust_value)
		temp_target = temp_gauge.value
		if (temp_gauge.value >= 100):
			adjust_increase = false
	elif (fixing and !adjust_increase):
		change = -3 * delta * level.getPanDifficulty()
		adjust_value += change
		adjust_value = clamp(adjust_value, 0, 100)
		temp_gauge.value = floor(adjust_value)
		temp_target = temp_gauge.value
		if (temp_gauge.value <= 0):
			adjust_increase = true
			
	if (temp.value < lower_bound * 100 or temp.value > upper_bound * 100):
		if (!known):
			noise.play()
			known = true
#		print(str(chef.name) + ": your pan sucks - temp.value = " + str(temp.value) + " lower_bound = " + str(lower_bound) + " upper_bound = " + str(upper_bound))
		stadium.lowerQuality(self)
		if (!wiggle_anim.is_playing()):
			wiggle_anim.play("Wiggle")
	elif (temp.value >= lower_bound * 100 or temp.value <= upper_bound * 100):
		if (known):
			known = false
			wiggle_anim.stop()
			appearance.set_offset(Vector2(0,0))

func setLevel(get_level):
	level = get_level
	pan_value *= level.getPanValue()
	lower_bound = level.getLowerBound()
	upper_bound = level.getUpperBound()
	temp.get_stylebox("slider").get_texture().set_gradient(level.getGradient())
	reset_timer = (randi() % level.getPanReset()) + level.getPanReset()
	temp.value = randi() % int(upper_bound * 100) + int(lower_bound * 100)
	temp_gauge.value = temp.value
	temp_target = temp_gauge.value
#	var replace = temp.get_stylebox("slider").get_texture().duplicate(true)
#	replace.set_gradient(level.getGradient())



func scrambleBurner():
	randomize()
	temp_target = randi() % 101
	temp_target_float = temp_gauge.value
	reset_timer = (randi() % level.getPanReset()) + level.getPanReset()

func changeBurner():
	fixing = true
	adjust_increase = true
	adjust_value = float(temp_gauge.value)

func stopAdjust():
	fixing = false
	emit_signal("finish")
	print(reset_timer)
	if(reset_timer <= 1.0):
		randomize()
		reset_timer = (randi() % level.getPanReset()) + level.getPanReset()
		print(reset_timer)

func attachStadium(scene):
	stadium = scene
	connect("finish", stadium, "finishDelay")

func checkAnim():
	if (temp.value < 30 and anim.is_playing()):
		anim.stop()
		appearance.frame = 0
	elif (temp.value >= 30 and temp.value <= 60):
		anim.play("Cooking")
	else:
		anim.play("Burning")

func increaseMultiplier(value):
	casting_multiplier = value

func resetMultiplier():
	casting_multiplier = 1.0

func createOffsets():
	randomize()
	lower_bound = max(randf(), 0.0)
	lower_bound = min(lower_bound, 1.0 - level.getPanBand())
	upper_bound = min(lower_bound + level.getPanBand(), 1.0)
	var return_val = PoolRealArray()
	return_val.append(0)
	return_val.append(lower_bound - 0.01)
	return_val.append(lower_bound)
	return_val.append(upper_bound)
	return_val.append(upper_bound + 0.01)
	return_val.append(1.0)
	
	return return_val

func debugGetOwner(get_owner):
	chef = get_owner

func getKnobStatus():
	if (temp_gauge.value > (lower_bound * 100) and temp_gauge.value < (upper_bound * 100)):
		return true
	else:
		return false

func getTempStatus():
	if (temp.value > (lower_bound * 100) and temp.value < (upper_bound * 100)):
		return true
	else:
		return false

func startParticles():
	$PanSprite/CPUParticles2D.set_emitting(true)

func stopParticles():
	$PanSprite/CPUParticles2D.set_emitting(false)