extends Resource

var difficulty = 10
var pot_multi = 1.75
var pot_difficulty = 1.75
var pan_band = 0.2
var knife_degrade = 5
var sharpening_value = 45
var num_items = 15
var cut_value = 10
var player_degrade = .05
var competitor_degrade = .05
var pan_reset = 8
var pan_value = 2
var pan_difficulty = 20
var pan_temp_change = 6
var wander = 1
var total_time = 90
var suspicion_delta = 1.5
var havoc_mult = 10
var distract_chance = 0.5
var distract_cooldown = 6
var lower_bound
var upper_bound
var new_gradient = Gradient.new()
var competitor_forgetfulness = 15
var competitor_delay = 2.0

func getDifficulty():
	return difficulty

func getPanBand():
	return pan_band

func getPotDifficulty():
	return pot_difficulty

func getPotMult():
	return pot_multi

func getCutValue():
	return cut_value

func getKnifeDegrade():
	return knife_degrade

func getPlayerDegrade():
	return player_degrade

func getCompetitorDegrade():
	return competitor_degrade

func getPanReset():
	return pan_reset

func getPanValue():
	return pan_value

func getPanDifficulty():
	return pan_difficulty

func getJudgeWander():
	return wander

func getTotalTime():
	return total_time

func getSuspicionDelta():
	return suspicion_delta

func getHavocMult():
	return havoc_mult

func getNumItems():
	return num_items

func getSharpeningValue():
	return sharpening_value

func getDistractChance():
	return distract_chance

func getDistractCooldown():
	return distract_cooldown

func getJudgeDistraction():
	pass

func getPanTempChange():
	return pan_temp_change

func createGradient():
	var color_array = PoolColorArray()
	
	color_array.append(Color.red)
	color_array.append(Color.red)
	color_array.append(Color.green)
	color_array.append(Color.green)
	color_array.append(Color.red)
	color_array.append(Color.red)
	
	new_gradient.set_colors(color_array)

	randomize()
	lower_bound = max(randf(), 0.0)
	lower_bound = min(lower_bound, 1.0 - getPanBand())
	upper_bound = min(lower_bound + getPanBand(), 1.0)
	
	var return_val = PoolRealArray()
	return_val.append(0)
	return_val.append(lower_bound - 0.01)
	return_val.append(lower_bound)
	return_val.append(upper_bound)
	return_val.append(upper_bound + 0.01)
	return_val.append(1.0)
	
	new_gradient.set_offsets(return_val)

func getGradient():
	return new_gradient

func getLowerBound():
	return lower_bound

func getUpperBound():
	return upper_bound

func getComptetitorForgetfulness():
	return competitor_forgetfulness

func getCompetitorDelay():
	return competitor_delay

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
