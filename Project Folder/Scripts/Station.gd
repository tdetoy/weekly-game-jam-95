extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setLevel(level):
	$Sprite/HBoxContainer/Cut.setLevel(level)
	$Sprite/HBoxContainer/Pan.setLevel(level)
	$Sprite/HBoxContainer/Pot.setLevel(level)
