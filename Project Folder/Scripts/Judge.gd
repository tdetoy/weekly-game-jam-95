extends Node2D

var level
var suspicion = false
var look_timer = 2.0
var distraction_timer = 1.0
var distracted = false
var stadium
var detecting = false
var dist
onready var appearance = $JudgeAppearance
onready var anim = $JudgeAppearance/AnimationPlayer
onready var noise = $JudgeAppearance/AudioStreamPlayer

signal caught_frame
signal secure
signal finish_distraction

func _ready():
	randomize()
	appearance.frame = randi() % 3 + 5
	set_process(true)

func _process(delta):
	if (distracted):
		distraction_timer -= delta
	if (!distracted and !detecting):
		look_timer -= delta
	if (look_timer <= 0.0 and !anim.is_playing()):
		randomize()
		var direction = randi() % 2
		if (direction == 0):
			if (appearance.frame == 6 or appearance.frame == 5):
				anim.play("LookLeft")
		elif (direction == 1):
			if (appearance.frame == 7 or appearance.frame == 5):
				anim.play("LookRight")
		look_timer = 2.0 + (randf() * level.getJudgeWander())
	if (appearance.frame == 7 and suspicion):
		emit_signal("caught_frame")
		if (!detecting):
			noise.play()
		detecting = true
	if (distraction_timer <= 0.0 and distracted):
		if (anim.get_current_animation() == "Talking"):
			distractedNoMore()
		elif (appearance.frame == 2):
			randomize()
			distraction_timer = 0 if randi() % 2 == 0 else 2 + level.getDistractCooldown() * randf()
			if (distraction_timer == 0.0 or suspicion):
				distractedNoMore()
			

func setLevel(get_level):
	level = get_level
	randomize()
	look_timer += randf() * level.getJudgeWander()
	

func enableDanger():
	suspicion = true

func disableDanger():
	detecting = false
	suspicion = false
	emit_signal("secure")

func getDistracted(distraction):
	distracted = true
	dist = distraction
	randomize()
	if (distraction == "talking"):
		anim.play("Talking")
	elif (distraction == "reading"):
		anim.play("LookDown")
		appearance.frame = 2
	distraction_timer = 3 + level.getDistractCooldown() * randf()

func distractedNoMore():
	distracted = false
	anim.stop()
	appearance.frame = randi() % 3 + 5
	if (dist == "talking"):
		emit_signal("finish_distraction")

func attachStadium(get_stadium):
	stadium = get_stadium
	connect("caught_frame", stadium, "increaseSuspicion")
	connect("secure", stadium, "safeAgain")
	connect("finish_distraction", stadium.getDistraction(), "finishDistraction")